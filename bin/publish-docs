#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo "No arguments supplied:"
    echo "Usage: $0 [tag|latest]"
    exit 1
fi

if [[ $1 = "-h" ]] || [[ $1 = "--help" ]] ; then
    echo "Usage: $0 [tag|latest]"
    exit 1
fi

VERSION=$1

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
DOCSDIR=$DIR/../docs/

echo "Docs location: $DOCSDIR"

# Prepare version
if [[ $VERSION = "latest" ]] ; then
    echo "Publishing latest version: master"
    CO=master
else
    if cd $DIR ; git rev-list $VERSION.. >/dev/null
    then
        echo "Tag: $VERSION"
        CO=$VERSION
    else
        echo "Tag $VERSION does not exist"
        exit 1
    fi
fi

git -C $DOCSDIR checkout $CO

# Targets
TARGETS=(html text epub)

for TARGET in ${TARGETS[*]}
do
    echo "Processing target: $TARGET"
    # Make 
    ( cd $DOCSDIR ; make $TARGET )

    # Copy to server
    echo "Copying directory $DOCSDIR/_build/$TARGET to tsagrista@mintaka:/dataB/gaiasky/docs/$TARGET/$VERSION"
    
    ssh tsagrista@mintaka "mkdir -p /dataB/gaiasky/docs/$TARGET/$VERSION"

    if [[ $TARGET = "epub" ]] ; then
        # Copy only epub
        ( cd $DOCSDIR/_build/$TARGET ; rsync -r *.epub tsagrista@mintaka:/dataB/gaiasky/docs/$TARGET/$VERSION )
    else
        # Copy all
        ( cd $DOCSDIR/_build/$TARGET ; rsync -r * tsagrista@mintaka:/dataB/gaiasky/docs/$TARGET/$VERSION )
    fi

done

# Update index files
echo "Updating index files"
ssh tsagrista@mintaka "/home/tsagrista/bin/dir2html '/dataB/gaiasky/docs/html/' 'docs/html' 'gaia.ari.uni-heidelberg.de/gaiasky' 'Gaia Sky documentation'"
ssh tsagrista@mintaka "/home/tsagrista/bin/dir2html '/dataB/gaiasky/docs/epub/' 'docs/epub' 'gaia.ari.uni-heidelberg.de/gaiasky' 'Gaia Sky documentation'"
ssh tsagrista@mintaka "/home/tsagrista/bin/dir2html '/dataB/gaiasky/docs/text/' 'docs/text' 'gaia.ari.uni-heidelberg.de/gaiasky' 'Gaia Sky documentation'"
ssh tsagrista@mintaka "/home/tsagrista/bin/dir2html '/dataB/gaiasky/docs/text/$VERSION/' 'text/$VERSION' 'gaia.ari.uni-heidelberg.de/gaiasky' 'Gaia Sky documentation'"

# Restore
git -C $DOCSDIR checkout master


