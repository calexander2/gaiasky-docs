# Gaia Sky docs

This project contains the documentation of Gaia Sky.

-  Latest documentation: http://gaia.ari.uni-heidelberg.de/gaiasky/docs/html/latest
-  Other formats and versions: http://gaia.ari.uni-heidelberg.de/gaiasky/docs
-  Readthedocs mirror: https://gaia-sky.readthedocs.io


Gaia Sky resources:

-  [Gaia Sky home page](https://zah.uni-heidelberg.de/institutes/ari/gaia/outreach/gaiasky/)
-  [Gaia Sky gitlab repository](https://gitlab.com/langurmonkey/gaiasky)
