Run instructions
****************

This section contains information on how to run the software depending on 
the install method.

Running regular installation
============================

If you installed the software using an installer or a package manager
system (``rpm``, ``deb``), you just need to use the standard running
procedures of your Operating System to run the application.

Linux
-----

Just type ``gaiasky`` in a terminal or use your favourite desktop
environment search engine to run the Gaia Sky launcher.

Windows
-------

In windows, this means clicking on ``Start`` and then browsing the start
menu folder ``Gaia Sky``. You can run the executable from there.

macOS
-----

Locate the launcher in your install directory (usually ``/Applications``) and double click on it.

**Code and pakcage**

However, if you are a maverick and do not like installers, you can also
run the Gaia Sky directly from the source code in ``GitHub`` or
using the ``tgz`` package.

Running from tar.gz package
===========================

You can also run the software using the ``tar.gz`` package without need for
an installation. To do so, extract the archive to your desired location and
run the ``gaiasky`` script (Linux, macOS), or the ``gaiasky.cmd`` batch file (Windows).

.. code:: console

    $  tar -xzvf gaiasky-[version].tar.gz
    $  cd gaiasky-[version]
    $  gaiasky #Linux/macOS
    $  .\gaiasky.cmd #Windows


.. _running-from-source:

Running from source
===================

Requirements
------------

If you want to compile the source code, you will need the following:

-  ``JDK11+``
-  ``git``

Please, be aware that only ``tags`` are guaranteed to work
(`here <https://gitlab.com/langurmonkey/gaiasky/tags>`__). The ``master``
branch holds the development version and the configuration files are
possibly messed up and not ready to work out-of-the-box. So remember to
use a ``tag`` version if you want to run it right away from source.

First, clone the repository:

.. code:: console

    $  git clone https://gitlab.com/langurmonkey/gaiasky.git

Getting the catalog data
------------------------

.. hint:: As of version ``2.1.0``, Gaia Sky provides a self-contained download manager to get all the data packs available.  

The ``base-data`` pack is necessary for Gaia Sky to run, and contains the Solar System, the Milky Way model, etc. Catalog files are optional but recommended if you want to see any stars at all. You can bring up the download manager at any time by clicking on the button ``Data download`` in the preferences window, data tab. More information on the download manager can be found in :ref:`download-manager`.

You can also download the data packs manually `here <http://gaia.ari.uni-heidelberg.de/gaiasky/files/autodownload>`_.

Compiling and running
---------------------

To compile the code and run Gaia Sky run the following.

.. code:: console

    $  gradlew core:run
    

**Tip**: Gaia Sky will check that you are using a version compatible with Java 11 when running the build. You can still use a newer JDK version (e.g. JDK 13) by setting the following environment variable to `false` in the context of gradle:

.. code:: console
    
    $  export GS_JAVA_VERSION_CHECK=false


In order to pull the latest changes from the GitHub repository:

.. code:: console

	$  git pull


CLI arguments
=============

Gaia Sky offers a few command line arguments. Run ``gaiasky -h`` for more information.

.. code:: console

    $  gaiasky -h
            
       Usage: gaiasky [options]
       Options:
          -h, --help
            Show program options and usage information.
          -v, --version
            List Gaia Sky version and relevant information.
            Default: false
          -d, --ds-download
            Display the data download dialog at startup. If no data is found, the 
            download dialog is shown automatically.
            Default: false
          -c, --cat-chooser
            Display the catalog chooser dialog at startup. This enables the 
            selection of different available catalogs when Gaia Sky starts.
            Default: false
          -p, --properties
            Specify the location of the properties file.
          -a, --assets
            Specify the location of the assets folder. If not present, the default 
            assets location (in the installation folder) is used.
          -vr, --openvr
            Launch in Virtual Reality mode. Gaia Sky will attempt creating a VR 
            context through OpenVR.
            Default: false
          -n, --noscript
            Do not start the scripting server. Useful to run more than one Gaia Sky 
            instance at once in the same machine.
            Default: false
          -e, --externalview
            Create a window with a view of the scene and no UI.
            Default: false


Packaging the software
======================

Gaia Sky can be exported to a folder to be run as a standalone app. Right now, doing so is only supported from Linux. You will need the utility ``help2man`` in your path. Remember to restart the gradle daemon after installing it.

.. code:: console

	$  gradlew core:dist
	
That will create a new folder called ``releases/gaiasky-[version].[revison]`` with the exported application. Run scripts are provided with the name ``gaiasky`` (Linux, macOS) and ``gaiasky.cmd`` (Windows).

Also, to export Gaia Sky into a ``tar.gz`` archive file, run the following.

.. code:: console

    $  gradlew core:createTar

In order to produce the desktop installers for the various systems you
need a licensed version of ``Install4j``. Then, you need to run:

.. code:: console

    $  gradlew core:pack

These command will produce the different OS packages (``.exe``, ``.dmg``, ``.deb``, ``.rpm``, etc.) 
of Gaia Sky into ``releases/packages-[version].[revision]`` folder.


