.. _folders:

System Folders
**************

In this documentation we will repeatedly refer to two different folders that Gaia Sky uses to store data and configuration settings: ``$GS_DATA`` and ``$GS_CONFIG``.
Their location depends on the Operating System and is specified below.

Linux
=====

As of version ``2.2.0``, the Linux release of Gaia Sky uses the `XDG base directory specification <https://specifications.freedesktop.org/basedir-spec/latest/>`__.

-  ``$GS_DATA`` = ``~/.local/share/gaiasky/``
-  ``$GS_CONFIG`` = ``~/.config/gaiasky/``

Windows and macOS
=================

Windows and macOS use the old ``.gaiasky`` folder in the user home directory for both locations, so:

-  ``$GS_DATA`` = ``$GS_CONFIG`` = ``[User.Home]/.gaiasky/``
