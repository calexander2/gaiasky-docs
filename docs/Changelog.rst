Changelog
*********

*  `Comprehensive version history <https://gitlab.com/langurmonkey/gaiasky/-/releases>`_
*  `Detailed changelog <https://gitlab.com/langurmonkey/gaiasky/blob/master/CHANGELOG.md>`_
*  `Full commit history <https://gitlab.com/langurmonkey/gaiasky/commits/master>`_
