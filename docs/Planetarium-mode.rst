Planetarium mode
****************

Gaia Sky supports a couple of planetarium modes, depending on the projector setup.

Single-projector setup
======================

Gaia Sky can output a true fisheye stream suitable for single-projector dome setups. If you need to separate the UI from the planetarium render window, you have two options:

*  Create an external view: :ref:`External views <external-views>`.
*  Connect two instances running (possibly) on different computers: :ref:`Connecting Gaia Sky instances <gaiasky-master-slave>`.

This planetarium mode runs by rendering the scene into a cubemap (using separate renders for all directions +X, -X, +Y, -Y, +Z, -Z) and renders it using a fisheye projection. A few things need to be considered:

-  **Cubemap side resolution** -- the resolution of each of the sides of the cubemap can be adjusted in the preferences window, planetarium mode tab.

-  **Aperture angle** -- the default aperture is 180, corresponding to a half-sphere. However this angle can be adjusted to suit different dome types in the planetarium mode tab of the preferences window.

-  **View skew** -- in focus mode, the view is skewed about 50 degrees downwards. This setting is not adjustable as of now.

.. hint:: To activate the planetarium mode, click on the |dome-icon| icon in the camera section of the controls window. Exit by clicking |dome-icon| again. You can also use the shortcut :guilabel:`CTRL` + :guilabel:`P`.

.. figure:: img/screenshots/planetarium-mode.jpg
  :width: 100%

  Planetarium mode

.. |dome-icon| image:: img/ui/dome-icon.png


.. _planetarium-multi-projector:

Multi-projector setup
=====================

Gaia Sky offers support for multi-projector setups, where a number of slave instances (each with its own viewport, field of view, warp and blend settings), are synchronized with a master (presenter) instance. Each slave is in charge of producing the image for a single projector and has a different view setup, geometry warp and blend mask. The current section only deals with the configuration of the view, warp and blend parameters for each slave. 

.. hint:: The configuration and setup of the connection between master and slave instances is documented in the :ref:`"Connecting Gaia Sky instances" section <gaiasky-master-slave>`. 

Additionally to the configration needed to connect master and slaves, the slave instances need a special view, resolution, warp and blend configuration. These depend on the specifications, location and orientation of each projector, as well as the projection surface. 

The following settings can be configured:

- The yaw angle -- turn the camera right
- The pitch angle -- turn the camera up
- The roll angle -- rotate the camera clock-wise
- The field of view angle
- The geometry warp file -- a PFM file that contains the destination location for each source location in normalized coordinates
- The blend mask -- an 8-bit RGB or RGBA PNG file with the blending mask

Gaia Sky offers two ways to configure these settings for each slave instance:

- Using the `MPCDI <https://newsandviews.dataton.com/what-is-mpcdi>`__ standard file format
- Using the configuration file of Gaia Sky directly

MPCDI
-----

Gaia Sky partially supports the ``MPCDI`` format in order to configure each instance. You will need a single ``.mpcdi`` file for each projector/gaia sky instance. Each file contains the resolution, the yaw, pitch and roll angles, the field of view angle and optionally a ``PFM`` warp file and a ``PNG`` blend mask. Gaia Sky does not support the ``MPCDI`` format fully, here are some caveats.

-  Only the '3d' profile is supported
-  Only one buffer per display is supported
-  Only one region per buffer is supported, and this region must cover the full frame
-  Only linear interpolation is supported for the warp file

In order to set the ``.mpcdi`` file for an instance, set/edit the following property in the instance's configuration file:

.. code:: jproperties

    program.net.slave.config=[path_to_file]/instance_config.mpcdi



Gaia Sky configuration file
---------------------------

If you do not have the ``MPCDI`` files for your projector setup, you can also configure each instance directly using the Gaia Sky properties file for that instance.

Usually, each instance has a configuration file with the name ``global.slave[n].properties``, without the brackets, where n is the slave number. Open this file for each instance and set/edit the following properties.

.. code:: jproperties

    # If you don't have an mpcdi file, use these next properties to
    # configure the orientation. In order for this to work, you also
    # need to set fullscreen=true, the right fullscreen resolution
    # and the right field of view angle.

    # Yaw angle (turn head right)
    program.net.slave.yaw=[yaw angle in degrees]
    # Pitch angle (turn head up)
    program.net.slave.pitch=[pitch angle in degrees]
    # Roll angle (rotate head cw)
    program.net.slave.roll=[roll angle in degrees]
    # Warp pfm file
    program.net.slave.warp=[path to PFM warp file]
    # Blend png file
    program.net.slave.blend=[path to 8-bit RGB or RGBA PNG file to use as blend mask]
