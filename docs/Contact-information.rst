About
*****

Contact
=======

If you have doubts or issues you can contact us using one of the
following methods.

-  Submit an issue to our `bug tracking system <http://gitlab.com/langurmonkey/gaiasky/issues>`__.
-  Drop us a line in `tsagrista@ari.uni-heidelberg.de <mailto:tsagrista@ari.uni-heidelberg.de>`__.

Do not forget to visit our `Homepage@ARI <http://www.zah.uni-heidelberg.de/gaia/outreach/gaiasky/>`__.

Author
======

**Toni Sagristà Sellés** -- `tonisagrista.com <http://tonisagrista.com>`__

Acknowledgements
================

The most up to date list of acknowledgements is always in the
`ACKNOWLEDGEMENTS.md <https://gitlab.com/langurmonkey/gaiasky/blob/master/ACKNOWLEDGEMENTS.md>`__ file.

Funding for the project is provided by the following agencies:

-  `ZAH <https://zah.uni-heidelberg.de>`__
-  `DLR <https://www.dlr.de>`__
-  `BMWi <https://www.bmwi.de>`__

