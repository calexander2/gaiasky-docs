.. _gaiasky-master-slave:

Connecting Gaia Sky instances
=============================

Gaia Sky offers a method to connect different instances together so that their 
internal state is synchronized. The model uses a **master-slave** 
scenario, where one (and only one) instance acts as a master and one or
more instances act as slaves, getting their internal states updated over
a network. The user interacts with the master and all slaves are updated accordingly.

This section describes only how to configure the master and the slave instances in order
to connect them together. This method is used to provide multi-projector rendering support
(i.e. planetarium domes), but extra steps are needed in order to configure the
orientation, distortion warp and blend settings for each slave instance.

.. hint:: Multi-projector configuration is covered in the :ref:`"Planetarium mode multi-projector setup" section <planetarium-multi-projector>`.

Configuration
-------------

The configuration is easy and painless. You will need to launch each instance of
Gaia Sky using a different configuration file (``global.properties``). You can
run Gaia Sky with a specific configuration file by using the ``-p`` or ``--properties``
command line flags:

.. code:: console

    $  gaiasky -p ~/.config/gaiasky/global.master.properties


The next sections explain how to configure the master and the slaves.


Configuration: slave instance(s)
--------------------------------

You can have as many slave instances as you want, but here we'll explain the process
of setting up two slaves.

1.  Copy the current ``global.properties`` file in your config folder (see :ref:`folders <folders>`)
    into ``global.slave0.properties``. The name is irrelevant, but choose something meaningful. 
    Repeat with ``global.slave1.properties``.
2.  Set the property ``program.net.slave=true`` in each file and make sure that ``program.net.master`` is set
    to ``false``. 
3.  Set the desired port to listen to in ``program.restport``. For example, to use the port 13900
    just set the property ``program.restport=13900``. Use a different port for each slave (i.e. slave 0 
    listens to 13900, slave 1 listens to 13901, etc.).

For exmaple, to set up a slave in port 13900, make sure that the following lines are in its configuration file:

.. code-block:: properties

    program.net.master=false
    program.net.slave=true
    program.restport=13900

**The slave instances should be launched before the master**. Launch the slave(s) with:

.. code:: console

    $  # Launch slave 0
    $  gaiasky -p /path/to/global.slave0.properties
    $  # Launch slave 1
    $  gaiasky -p /path/to/global.slave1.properties


Once the slave(s) have been launched, you can verify that the API is working by
visiting ``http://localhost:13900/api/help`` with your browser. 
Modify the port with whatever port you are using.

.. hint:: Only the master is starting the scripting server. The slaves are automatically forbidden to do so!

Configuration: master instance
------------------------------

Copy the current ``global.properties`` file into ``global.master.properties`` and 
edit the following lines.

1.  Set the property ``program.net.master=true`` and make sure that ``program.net.slave``
    is set to ``false``.
2.  Add the locations of all desired slaves under the properties ``program.net.master.slaves.n=URL``,
    where ``n`` is an integer that goes from 0 to the number of slaves - 1.


For example, in order to connect the master with two slaves, both running locally (``localhost``) on ports
13900 and 13901, add the following to the ``global.master.properties`` file:

.. code-block:: properties

    program.net.master=true
    program.net.slave=false
    program.net.master.slave.0=http://localhost\:13900/api/
    program.net.master.slave.1=http://localhost\:13901/api/


Then, just launch the master (**after the slaves are running!**):

.. code:: console

    $  gaiasky -p /path/to/global.master.properties



Caveats
-------

Even though this offers a very flexible system to connect several instances of Gaia Sky
together, each instance is a fully-fledged application with its own copy of 
the scene graph and the data structures. 
This means that, if you run them locally, the data and scene graph will
be replicated several times in memory, possibly consuming lots of gigabytes. 

Handle it with care.
