.. _datasets:

Datasets
********

Gaia Sky supports the loading and visualization of datasets of different 
nature.

Loading datasets
================

Datasets can be loaded into Gaia Sky by three different means:

*  Via SAMP (see :ref:`this section <samp>`).
*  Via scripting (see :ref:`this section <scripting-datasets>`).
*  Directly using the UI. See the next paragraph.

In order to load a dataset, click on the folder icon |load-dataset-icon| in the controls
window and choose the file you want to load. Right now, ``.csv`` (Comma-Separated Values) and 
``.vot`` (VOTable) formats are supported through the STIL library. Once the dataset has been chosen, a
new window is presented to the user asking the type of the dataset and some extra options associated with it. This window is also presented when loading a dataset via SAMP.

Datasets can be **star** datasets or **particle** datasets,
depending on whether the new dataset contains stars (with magnitudes, colors, proper motions and whatnot)
or just particles (only 2D or 3D positions and extra attributes).

Please, see the :ref:`STIL data provider section <stil-data-provider>` for more information on how to 
prepare the datasets for Gaia Sky.

Star datasets
~~~~~~~~~~~~~

Star datasets are expected to contain some attributes of stars, like magnitudes, color indices, proper motions, etc., and use the regular star shaders to render the stars. When selecting star datasets, there are a couple of settings available:

*  **Dataset name** -- the name of the dataset.
*  **Magnitude scale factor** -- subtractive scaling factor to apply to the magnitude of all stars (``appmag = appmag - factor``).
*  **Label color** -- the color of the labels of the stars in the dataset.
*  **Fade in** -- these are two distances from the Sun, in parsecs, that will be used as interpolation limits to fade in the whole dataset. The dataset will not be visible if the camera distance from the Sun is smaller than the lower limit, and it will be fully visible if the camera distance from the Sun is larger than the upper limit. The opacity is interpolated between 0 and 1 if the camera distance from the Sun is larger than the lower limit and smaller than the upper limit.
*  **Fade out** -- these are two distances from the Sun, in parsecs, that will be used as interpolation limits to fade out the whole dataset. The dataset will not be visible if the camera distance from the Sun is larger than the upper limit, and it will be fully visible if the camera distance from the Sun is smaller than the lower limit. The opacity is interpolated between 1 and 0 if the camera distance from the Sun is larger than the lower limit and smaller than the upper limit.

.. figure:: img/ui/ds-stars.jpg

 Star dataset loading options

Particle datasets
~~~~~~~~~~~~~~~~~

Particle datasets only require positions to be present, and use generic shaders to render the particles. Some parameters can be tweaked at load time to control the appearance and visibility of the particles:

*  **Dataset name** -- the name of the dataset.
*  **Profile decay** -- a power that controls the radial profile of the actual particles, as in ``(1-d)^pow``, where ``d`` is the distance from the center to the edge of the particle, in [0,1].
*  **Particle color** -- the color of the particles. Can be modified with the particle color noise.
*  **Particle color noise** -- a value in [0,1] that controls the amount of noise to apply to the particle colors in order to get slightly different colors for each particle in the dataset.
*  **Label color** -- color of the label of this dataset. Particles themselves do not have individual labels.
*  **Particle size** -- the size of the particles, in pixels.
*  **Component type** -- the component type to apply to the particles to control their visibility. Make sure that the chosen component type is enabled in the Visibility pane.
*  **Fade in** -- these are two distances from the Sun, in parsecs, that will be used as interpolation limits to fade in the whole dataset. The dataset will not be visible if the camera distance from the Sun is smaller than the lower limit, and it will be fully visible if the camera distance from the Sun is larger than the upper limit. The opacity is interpolated between 0 and 1 if the camera distance from the Sun is larger than the lower limit and smaller than the upper limit.
*  **Fade out** -- these are two distances from the Sun, in parsecs, that will be used as interpolation limits to fade out the whole dataset. The dataset will not be visible if the camera distance from the Sun is larger than the upper limit, and it will be fully visible if the camera distance from the Sun is smaller than the lower limit. The opacity is interpolated between 1 and 0 if the camera distance from the Sun is larger than the lower limit and smaller than the upper limit.

.. figure:: img/ui/ds-particles.jpg

Star cluster datasets
~~~~~~~~~~~~~~~~~~~~~

Star cluster catalogs can also be loaded directly from the UI as of Gaia Sky 2.2.6. The loader also uses STIL to load CSV or VOTable files. In CSV mode the units are fixed, otherwise they are read from the VOTable, if it has them. The order of the columns is not important. The required columns are the following:

-  ``name``, ``proper``, ``proper_name``, ``common_name``, ``designation`` -- one or more name strings, separated by '|'.
-  ``ra``, ``alpha``, ``right_ascension`` -- right ascension in degrees.
-  ``dec``, ``delta``, ``de``, ``declination`` -- declination in degrees.
-  ``dist``, ``distance`` -- distance to the cluster in parsecs, or
-  ``pllx``, ``parallax`` -- parallax in mas, if distance is not provided.
-  ``rcluster``, ``radius`` -- the radius of the cluster in degrees.

Optional columns, which default to zero, include:

-  ``pmra``, ``mualpha``, ``pm_ra`` -- proper motion in right ascension, in mas/yr.
-  ``pmdec``, ``mudelta``, ``pm_dec`` -- proper motion in declination, in mas/yr.
-  ``rv``, ``radvel``, ``radial_velocity`` -- radial velocity in km/s.


Star cluster datasets require positions and radii to be present, and use wireframe spheres to render the clusters. The parameters that can be tweaked at load time are:

*  **Dataset name** -- the name of the dataset.
*  **Particle color** -- the color of the clusters and their labels.
*  **Label color** -- color of the label of this dataset. Particles themselves do not have individual labels.
*  **Component type** -- the component type to apply to the particles to control their visibility. Make sure that the chosen component type is enabled in the Visibility pane.
*  **Fade in** -- these are two distances from the Sun, in parsecs, that will be used as interpolation limits to fade in the whole dataset. The dataset will not be visible if the camera distance from the Sun is smaller than the lower limit, and it will be fully visible if the camera distance from the Sun is larger than the upper limit. The opacity is interpolated between 0 and 1 if the camera distance from the Sun is larger than the lower limit and smaller than the upper limit.
*  **Fade out** -- these are two distances from the Sun, in parsecs, that will be used as interpolation limits to fade out the whole dataset. The dataset will not be visible if the camera distance from the Sun is larger than the upper limit, and it will be fully visible if the camera distance from the Sun is smaller than the lower limit. The opacity is interpolated between 1 and 0 if the camera distance from the Sun is larger than the lower limit and smaller than the upper limit.

.. figure:: img/ui/ds-clusters.jpg


Managing datasets
=================


.. figure:: img/datasets/dataset-controls.png

  Dataset entry in the controls window

All datasets currently loaded are displayed in the `Datasets` pane of the controls window. 
A few actions can be carried out directly from that pane:

*  |eye-s-on| -- toggle the visibility of the dataset
*  |prefs-s-icon| -- open the dataset preferences window
*  |bin-s-icon| -- delete (unload) dataset
*  |highlight-s-off| -- highlight the dataset using the current color and particle size. The **color** can be changed by clicking on the icon just above of this icon (blue square in the image), and the particle size factor can be adjusted in the dataset preferences window.

.. _dataset-hl-color:

Dataset highlight color
~~~~~~~~~~~~~~~~~~~~~~~

Above the |highlight-s-off| icon is the color icon. Use it to define the highlight color for the dataset. The color can either be a plain color or a color map. 

A plain color can be chosen using the color picker dialog that appears when clicking on the "Plain color" radio button.

.. figure:: img/datasets/colorpicker-color.png

  The plain color picker dialog

The "Color map" radio button displays the screen shown below. From there, you can choose the color map type, as well as the attribute to use for the mapping and the maximum and minimum mapping values.

The **available attributes** depend on the dataset type and loading method. Particle datasets have coordinate attributes (right ascension, declination, ecliptic longitude and latitude, galactic longitude and latitude) and distance distance. Star datasets have, additionally, apparent and absolute magnitudes, proper motions (in alpha and delta) and radial velocity. For all datasets loaded from VOTable either directly or through SAMP, all the numeric attributes are also available

.. figure:: img/datasets/colorpicker-cmap.png

  The color map dialog


Dataset preferences
-------------------

Clicking on the preferences icon |prefs-s-icon|, a few extra settings can be adjusted:

*  **Size increase factor** - scale factor to apply to the particles when the dataset is highlighted.
*  **Make all particles visible** - raises the minimum opacity to a non-zero value when the dataset is highlighted.
*  **Filters** - allows for the creation of arbitrary selection filters by setting conditions (rules) on the particle attributes. Several rules can be defined, but only one type of logical operator (AND, OR) is possible. The available attributes depend on the dataset and are covered in :ref:`the dataset color settings <dataset-hl-color>` 


.. |load-dataset-icon| image:: img/ui/open-icon.png
.. |eye-s-on| image:: img/ui/eye-s-on.png
.. |prefs-s-icon| image:: img/ui/prefs-s-icon.png
.. |bin-s-icon| image:: img/ui/bin-s-icon.png
.. |highlight-s-off| image:: img/ui/highlight-s-off.png
