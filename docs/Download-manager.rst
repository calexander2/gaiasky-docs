.. _download-manager:

Download manager
================

As of version ``2.1.0`` Gaia Sky provides an integrated download manager to help visualize and obtain the
available data packs and catalogs. Chances are that the download manager is the first thing you see when you launch
Gaia Sky for the first time.

.. figure:: img/dm/dm.gif
  :alt: The download manager in action
  :width: 100%

  The download manager in action
  
The download manager pops up automatically when Gaia Sky is started if no base data or no catalog files are detected. It
can also be launched manually from the preferences window, data tab.

Using the download manager, the user can select whatever datasets she wants, then click download and wait for the download and
extract processes to finish. Once done, the data will be available to Gaia Sky the next time it starts.
