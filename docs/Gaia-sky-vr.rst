.. _gaiasky-vr:

Gaia Sky VR
===========

`Gaia Sky VR <https://zah.uni-heidelberg.de/gaia/outreach/gaiasky>`__ is the VR
version of Gaia Sky. It runs on multiple headsets and operating systems
thanks to Valve's `OpenVR <https://github.com/ValveSoftware/openvr>`__,
also implemented by `OpenOVR <https://gitlab.com/znixian/OpenOVR>`__. It
is developed in the framework of `ESA <http://www.esa.int/ESA>`__'s
`Gaia mission <http://sci.esa.int/gaia>`__ to chart about 1 billion
stars of our Galaxy.

Running Gaia Sky VR
-------------------


The Gaia Sky VR project is the Virtual Reality version of Gaia Sky. At
the moment, only `OpenVR <https://github.com/ValveSoftware/openvr>`__ is
supported. Our tests have been carried out with the
**Oculus Rift CV1** headset in under Windows and the **Valve Index** on Windows
and Linux. Both work reasonably well.

Currently, the regular installation of Gaia Sky also includes the VR version.
On windows, you can run it using the ``gaiaskyvr.exe`` file. On Linux, just use
the `-vr` parameter.

Pre-requisites
^^^^^^^^^^^^^^

The minimum system requirements for running Gaia Sky VR are as
following:

+------------------------+----------------------------------------------------------------------------------------+
| **VR Headset**         | `OpenVR <https://en.wikipedia.org/wiki/OpenVR>`__-compatible (Oculus Rift, HTC Vive)   |
+------------------------+----------------------------------------------------------------------------------------+
| **Operating system**   | Linux (only supported headsets) / Windows 10                                           |
+------------------------+----------------------------------------------------------------------------------------+
| **CPU**                | Intel Core i5 3rd Generation or similar. 4 core or higher recommended                  |
+------------------------+----------------------------------------------------------------------------------------+
| **GPU**                | VR-capable GPU (GTX 970 or above)                                                      |
+------------------------+----------------------------------------------------------------------------------------+
| **Memory**             | 8+ GB RAM                                                                              |
+------------------------+----------------------------------------------------------------------------------------+
| **Hard drive**         | 1 GB of free disk space (depending on datasets)                                        |
+------------------------+----------------------------------------------------------------------------------------+

Software-wise, you will need the following:

1. Follow the provided vendor instructions and install the Oculus app
   with the runtime. If using a SteamVR headset (HTC Vive/Pro, Valve
   Index, etc.), just get Steam and download SteamVR.
2. For the Oculus Rift, you need a translation layer from OpenVR to LibOVR.
   You can either use SteamVR (slower) or OpenOVR (faster). We recommend
   using OpenOVR, as it is much simpler and faster.

   1. **OpenOVR OpenComposite** - Download `OpenOVR's OpenComposite
      Launcher <https://gitlab.com/znixian/OpenOVR>`__, launch it and
      select 'Switch to OpenComposite'. That's it.
   2. **SteamVR** - Download and install
      `Steam <http://store.steampowered.com/>`__ and then install
      `SteamVR <http://store.steampowered.com/steamvr>`__ and launch it.
      The SteamVR runtime must be running alongside the Oculus Runtime
      for it to work.

4. `OpenJDK 11+ <https://jdk.java.net/java-se-ri/11>`__.
5. A `VR-ready rig <https://www.digitaltrends.com/virtual-reality/how-to-build-a-cheap-vr-ready-pc/>`__.


Windows
~~~~~~~
The easiest way to get it running in Windows is to install the latest 
version of Gaia Sky and directly run the executable ``gaiaskyvr.exe`` file. You should also have
a start menu entry called 'Gaia Sky VR', if you chose to create it during
the installation.


Linux
~~~~~
Download and install Gaia Sky, and then run:

.. code:: console
    
    $  gaiasky -vr



Getting the data
----------------

You can use the same data folder for the VR and desktop versions.

The download manager should show up automatically at startup. If it does not, force it
with the ``-d`` argument (or using ``gradlew core:rund`` if running
from sources). Just select the data packs and catalogs that you want
to download, press ``Download now`` and wait for the process to finish.

.. code:: console

    $  gaiasky -d

You can also download the **data packs manually**
`here <http://gaia.ari.uni-heidelberg.de/gaiasky/files/autodownload/>`__.

Controls
--------

On the Oculus Rift controller the mappings are as follow:

-  **Joystick (move)** - move around.
-  **Trigger** - hold it to select an object and enter focus mode.
-  **Jystick (push)** - return to free mode.
-  **A** or **X** - toggle visibility of labels
-  **B** or **Y** - enable/disable on-screen information
-  **A** + **B** or **X** + **Y** - show usage info on screen
-  **Grip** + **Joystick (move)** - rotate around focus, only in focus
   mode

Common problems
---------------

-  If you are using an Optimus-powered laptop, make sure that the
   ``java.exe`` you are using to run Gaia Sky VR is `set up properly in
   the Nvidia Control
   Panel <https://www.pcgamer.com/nvidia-control-panel-a-beginners-guide/>`__
   to use the discrete GPU.
-  If you experience low frame rates with an Oculus headset, try using
   OpenOVR OpenComposite instead of SteamVR.
-  If you don't see the VR controllers, check the output log for a line
   that starts with ``VRContext - WARN: Could not [...]`` and attach it
   or the full log file to a `bug
   report <https://gitlab.com/langurmonkey/gaiasky/issues>`__.
-  Make sure you are using Java 11+.

More info
---------

The project's VR file is `here <https://gitlab.com/langurmonkey/gaiasky/blob/master/VR.md>`__.
