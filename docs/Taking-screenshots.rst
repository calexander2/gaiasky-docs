Taking screenshots
******************

Gaia Sky has an in-built screenshot capturing feature. To take a
screenshot press :guilabel:`F5` any time during the execution of the program. By
default, screenshots are saved in the ``$GS_DATA/screenshots`` (see :ref:`folders <folders>`)
folder. The screenshots are in ``PNG`` format with high quality
settings, so they can grow quite big if the resolution is large.

.. hint:: Take a screenshot with :guilabel:`F5`.

Screenshot modes
================

-  **Simple mode** - This mode saves the current screen buffer to a
   file. It captures also the GUI and it does so at the current display
   resolution.
-  **Advanced mode** - This mode renders the current scene to an
   off-screen buffer with an arbitrary resolution. The resolution can be
   configured in the configuration dialog, ``Screenshots`` tab. The advanced
   mode will **NOT** capture the GUI or any additional elements that are
   not part of the scene.
