Gaia Sky documentation
----------------------

|Build status| |License|


These are the official documentation pages of Gaia Sky. Find below the contents table to navigate around.

-  Visit our `home page <http://www.zah.uni-heidelberg.de/gaia/outreach/gaiasky/>`__
-  Download `Gaia Sky <http://www.zah.uni-heidelberg.de/gaia/outreach/gaiasky/downloads/>`__
-  Submit a `bug or a feature request <https://gitlab.com/langurmonkey/gaiasky/issues>`__
-  Follow development news at `@GaiaSky_Dev <https://twitter.com/GaiaSky_Dev>`__


Gaia Sky is described in the paper `Gaia Sky: Navigating the Gaia Catalog <http://dx.doi.org/10.1109/TVCG.2018.2864508>`__.

.. figure:: img/header2.jpg
   :alt: Gaia Sky dramatic image


.. |Build status| image:: https://circleci.com/gh/langurmonkey/gaiasky.svg?style=shield&circle-token=:circle-token
   :target: https://circleci.com/gh/langurmonkey/gaiasky/tree/master

.. |License| image:: https://img.shields.io/badge/License-MPL%202.0-brightgreen.svg
   :target: https://opensource.org/licenses/MPL-2.0

Contents
========

.. toctree::
   :maxdepth: 3

   Installation
   Folders
   Running-gaia-sky
   Usage-data-options
   Additional-information
   Gaia-sky-vr
   Tutorials-workshops
   FAQ
   Javadoc
   Changelog
   Contact-information
