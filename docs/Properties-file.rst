.. _properties-file:

The configuration file
**********************

There is a configuration file which stores most of the configration settings
of Gaia Sky. This section is devoted
to these seetings that are not represented in the GUI but are still
configurable. The configuration file is located in
``$GS_CONFIG/global.properties`` (see :ref:`folders <folders>`). The file is annotated with
comments specifying the function of most properties. However, here is an 
explanation of some of the properties
found in this file that are not represented in the GUI.


-  **data.limit.mag** -- this contains the limiting magnitude above which
   stars shall not be loaded. Not all data loaders implement this. It is
   now deprecated.
-  **scene.octree.maxstars** -- the maximum number of stars loaded at a time
   from LOD (octree-backed) catalogs. Increase this to allow for more stars 
   to be loaded. When this number is hit, the application unloads old, unobserved
   octants.
-  **program.debuginfo** -- if this property is set to true, some debug
   information will be shown at the top right of the window. This
   contains information such as the number of stars rendered as a quad,
   the number of stars rendered as a point or the frames per second.
   This can be activated in real time by pressing :guilabel:`Ctrl` + :guilabel:`d`.
-  **controls.blacklist** -- list of game controllers to ignore.
