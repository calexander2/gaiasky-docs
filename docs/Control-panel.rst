.. _user-interface:

Control panel
*************

The Gaia Sky control panel is divided into seven panes, `Time <#time>`__,
`Camera <#camera>`__, `Type visibility <#type-visibility>`__, `Visual settings <#visual-settings>`__, `Datasets <#datasets>`__, `Objects <#objects>`__,  and `Music <#music>`__.

+----------------------------------------------------+-------------------------------------------------------+
| .. image:: img/ui/control-panel-collapsed.jpg      | .. image:: img/ui/control-panel-expanded.jpg          |
|   :width: 45%                                      |   :width: 45%                                         |
|   :alt: User interface with all panes collapsed    |   :alt: User interface with camera pane expanded      |
+----------------------------------------------------+-------------------------------------------------------+            
| Collapsed control panel                            | Expanded control panel                                |
+----------------------------------------------------+-------------------------------------------------------+


The seven panes, except for the Time pane, are collapsed at startup. To expand them and reveal its controls just click on the little plus 
icon |plus-icon| at the right of the pane title. Use the minus icon |minus-icon| to collapse it again. Panes can also be detached
to their own window. To do so, use the detach icon |detach-icon|.

.. |plus-icon| image:: img/ui/plus-icon.png
.. |minus-icon| image:: img/ui/minus-icon.png
.. |detach-icon| image:: img/ui/detach-icon.png

Time
====

You can play and pause the simulation using the |play-icon|/|pause-icon| ``Play/Pause`` buttons in
the ``Controls`` window to the left. You can also use :guilabel:`Space` to play
and pause the time. You can also change time warp, which is expressed as
a factor. Use :guilabel:`,` and :guilabel:`.` to divide by 2 and double the value of the
time warp.

.. |play-icon| image:: img/ui/play-icon.png
.. |pause-icon| image:: img/ui/pause-icon.png

Camera
======

In the camera options pane on the left you can select the type of
camera. This can also be done by using the :guilabel:`Numpad 0-7` keys.

There are eight camera modes:

* **Free mode** -- the camera is not linked to any object and its velocity is exponential with respect to the distance to the origin (Sun).
* **Focus mode** -- the camera is linked to a focus object and it rotates and rolls with respect to it.
* **Game mode** -- a game mode which maps the controls :guilabel:`wasd` + mouse look.
* **Gaia scene** -- outside view of the Gaia satellite. The camera can not be rotated or translated in this mode.
* **Spacecraft**-- take control of a spacecraft and navigate around at will.
* **Gaia FOV (1, 2, both)** -- the camera simulates either of the fields of view of Gaia, or both.

For more information on the camera modes, see the :ref:`camera-modes` section.

Additionally, there are a number of sliders for you to control different
parameters of the camera:

-  **Field of view** -- control the field of view angle of the camera.
   The bigger it is, the larger the portion of the scene represented.
-  **Camera speed** -- control the longitudinal speed of the camera.
-  **Rotation speed** -- control the transversal speed of the camera, how
   fast it rotates around an object.
-  **Turn speed** -- control the turning speed of the camera.

You can **lock the camera** to the focus when in focus mode. Doing so
links the reference system of the camera to that of the object and thus
it moves with it.

.. hint:: **Lock the camera** so that it stays at the same relative position to the focus object.

Finally, we can also **lock the orientation** of the camera to that of
the focus so that the same transformation matrix is applied to both.

.. hint:: **Lock the orientation** so that the camera also rotates with the focus.

Additionally, we can also enable the **crosshair**, which will mark the
currently focused object.

Type visibility
===============

Most graphical elements can be turned off and on using these toggles.
For example you can remove the stars from the display by clicking on the
``stars`` toggle. The object types available are the following:

-  |stars| -- Stars
-  |planets| -- Planets
-  |moons| -- Moons
-  |satellites| -- Satellites
-  |asteroids| -- Asteroids
-  |clusters| -- Star clusters
-  |milkyway| -- Milky Way
-  |galaxies| -- Galaxies
-  |nebulae| -- Nebulae
-  |meshes| -- Meshes
-  |equatorial| -- Equatorial grid
-  |ecliptic| -- Ecliptic grid
-  |galactic| -- Galactic grid
-  |labels| -- Labels
-  |titles| -- Titles
-  |orbits| -- Orbits
-  |locations| -- Locations
-  |countries| -- Countries
-  |constellations| -- Constellations
-  |boundaries| -- Constellation boundaries
-  |ruler| -- Rulers
-  |effects| -- Particle effects
-  |atmospheres| -- Atmospheres
-  |clouds| -- Clouds
-  |axes| -- Axes
-  |arrows| -- Velocity vectors
-  |others| -- Others

.. |stars| image:: img/ui/ct/icon-elem-stars.png
.. |planets| image:: img/ui/ct/icon-elem-planets.png
.. |moons| image:: img/ui/ct/icon-elem-moons.png
.. |satellites| image:: img/ui/ct/icon-elem-satellites.png
.. |asteroids| image:: img/ui/ct/icon-elem-asteroids.png
.. |clusters| image:: img/ui/ct/icon-elem-clusters.png
.. |milkyway| image:: img/ui/ct/icon-elem-milkyway.png
.. |galaxies| image:: img/ui/ct/icon-elem-galaxies.png
.. |nebulae| image:: img/ui/ct/icon-elem-nebulae.png
.. |meshes| image:: img/ui/ct/icon-elem-meshes.png
.. |equatorial| image:: img/ui/ct/icon-elem-equatorial.png
.. |ecliptic| image:: img/ui/ct/icon-elem-ecliptic.png
.. |galactic| image:: img/ui/ct/icon-elem-galactic.png
.. |labels| image:: img/ui/ct/icon-elem-labels.png
.. |titles| image:: img/ui/ct/icon-elem-titles.png
.. |orbits| image:: img/ui/ct/icon-elem-orbits.png
.. |locations| image:: img/ui/ct/icon-elem-locations.png
.. |countries| image:: img/ui/ct/icon-elem-countries.png
.. |constellations| image:: img/ui/ct/icon-elem-constellations.png
.. |boundaries| image:: img/ui/ct/icon-elem-boundaries.png
.. |ruler| image:: img/ui/ct/icon-elem-ruler.png
.. |effects| image:: img/ui/ct/icon-elem-effects.png
.. |atmospheres| image:: img/ui/ct/icon-elem-atmospheres.png
.. |clouds| image:: img/ui/ct/icon-elem-clouds.png
.. |axes| image:: img/ui/ct/icon-elem-axes.png
.. |arrows| image:: img/ui/ct/icon-elem-arrows.png
.. |others| image:: img/ui/ct/icon-elem-others.png

.. _individualvisibility:

Individual visibility
---------------------

This button provides access to controls to manipulate the individual visibility of
some non-selectable object types like meshes and constellations.

.. figure:: img/screenshots/individual-visibility.jpg
  :alt: Individual object visibility

  Individual object visibility button and dialog

As shown in the image above, when clicking the "Individual visibility" button, a new
dialog appears, from which individual meshes and constellations can be selected and
unselected. Unselected objects will not be rendered.

.. _velocityvectors:

Velocity vectors
----------------

Enabling **velocity vectors** activates the
representation of star velocities, if the currently loaded catalog
provides them. Once velocity vectors are activated, a few extra controls become
available to tweak their length and color.

.. figure:: img/screenshots/velocity-vectors.jpg
  :alt: Velocity vectors in Gaia Sky
  :width: 100%

  Velocity vectors in Gaia Sky

*  **Number factor** -- control how many velocity vectors are rendered. The stars are sorted by magnitude (ascending) so the brightest stars will get velocity vectors first.
*  **Length factor** -- length factor to scale the velocity vectors.
*  **Color mode** -- choose the color scheme for the velocity vectors:
  
    *  **Direction** -- color-code the vectors by their direction. The vectors :math:`\vec{v}` are pre-processed (:math:`\vec{v}'=\frac{|\vec{v}|+1}{2}`) and then the :math:`xyz` components are mapped to the colors :math:`rgb`.
    *  **Speed** -- the speed is normalized in from :math:`[0,100] Km/h` to :math:`[0,1]` and then mapped to colors using a long rainbow colormap (see `here <http://www.particleincell.com/blog/2014/colormap/>`__).
    *  **Has radial velocity** -- stars in blue have radial velocity, stars in red have no radial velocity.
    *  **Redshift from the Sun** -- map the redshift (radial velocity) from the sun using a red-to-blue colormap.
    *  **Redshift from the camera** -- map the redshift (radial velocity) from the current camera position using a red-to-blue colormap.
    *  **Solid color** -- use a solid color for all arrows.

*  **Show arrowheads** -- Whether to show the vectors with arrow caps or not.

.. hint:: Control the width of the velocity vectors with the **line width** slider in the **visual settings** pane.

.. _interface-lighting:
.. _visual-settings:

Visual settings
===============

Here are a few options to control the lighting of the scene:

-  **Star brightness** -- control the brightness of stars.
-  **Star size (px)** -- control the size of point-like stars.
-  **Min. star opacity** -- set a minimum opacity for the faintest stars.
-  **Ambient light** -- control the amount of ambient light. This only
   affects the models such as the planets or satellites.
-  **Line width** -- control the width of all lines in Gaia Sky (orbits, velocity vectors, etc.).
-  **Label size** -- control the size of the labels.
-  **Elevation multiplier** -- scale the height representation.

Bookmarks
=========

Gaia Sky offers a bookmark system to keep your favorite objects
organized and at hand. This panel centralizes the operation of
bookmarks. You can find more information on this in the
:ref:`bookmarks section <bookmarks>`.

Datasets
========

This tab contains all the datasets currently loaded. For each dataset, a highlight
color can be defined. The dataset properties window can be used to define 
arbitrary filters on any of the properties of the elements of the dataset.
Datasets can be highlighted by clicking on the little crosshair next to the name,
below the color picker.

Please see the :ref:`datasets section <datasets>` for more 
information on this.

Music
=====

Since version ``0.800b``, Gaia Sky also offers a music player in its
interface. By default it ships with only a few *spacey* melody, but you
can add your own by dropping them in the folder ``$GS_DATA/music`` (see :ref:`folders <folders>`).

.. hint:: Drop your ``mp3``, ``ogg`` or ``wav`` files in the folder ``$GS_DATA/music`` and these will be available during your Gaia Sky sessions to play.

In order to start playing, click on the |audio-play| ``Play`` button. To pause the track, click on the |audio-pause| ``Pause`` icon. To skip to the next track,
click on the |audio-fwd| ``Forward`` icon. To go to the previous track, click on the |audio-bwd| ``Backward`` icon.
The volume can be controlled by scrolling up and down, using the mouse wheel, over the volume indicator. The default volume value is 5%.

.. |audio-play| image:: img/ui/audio-play.png
.. |audio-pause| image:: img/ui/audio-pause.png
.. |audio-fwd| image:: img/ui/audio-fwd.png
.. |audio-bwd| image:: img/ui/audio-bwd.png


Bottom buttons
==============

The buttons at the bottom of the control panel are described here.

Minimap
-------

With this button |minimap| you can toggle the minimap window on an off. You can also use :guilabel:`Tab`.

.. |minimap| image:: img/ui/map-icon.png

Load dataset
------------

Use this button |dsload| to load a new VOTable file (``.vot``) into Gaia Sky. Refer to the :ref:`stil-data-provider` section for more information on the metadata needed for Gaia Sky to parse the dataset correctly.

.. |dsload| image:: img/ui/open-icon.png

Preferences window
------------------

You can launch the preferences window any time during the execution of
the program. To do so, click on the |prefsicon| ``Preferences`` button at the bottom
of the GUI window. For a detailed description of the configuration
options refer to the :ref:`Configuration
Instructions <configuration>`.

.. |prefsicon| image:: img/ui/prefs-icon.png

.. _running-scripts:

Log
---

The log button |logicon| brings up the log window, which displays the Gaia Sky log
for the current session. The log can be exported to a file by clicking on the ``Export to
file`` button. The location of the exported log files is ``$GS_DATA`` (see :ref:`folders <folders>`).

.. |logicon| image:: img/ui/log-icon.png

About/help
----------

The help button |helpicon| brings up the help dialog, where information on the current system,
OpenGL settings, Java memory, updates and contact can be found.

.. |helpicon| image:: img/ui/help-icon.png


Exit
----

Click on the cross icon to exit Gaia Sky. You can also use :guilabel:`Esc`.
