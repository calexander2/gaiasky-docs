.. _controls:

Controls
********

This section describes the controls of Gaia Sky.

Keyboard controls
=================

To check the most up-to-date controls go to the ``Controls`` tab in the
preferences window. Here are the default keyboard controls depending on the
current camera mode. Learn more about camera modes in the :ref:`camera-modes` section.

Keyboard mappings
-----------------

The keyboard mappings are stored in an internal file called ``keyboard.mappings`` (`link <https://gitlab.com/langurmonkey/gaiasky/blob/master/assets/mappings/keyboard.mappings>`__).
If you want to edit the keyboard mappings, copy the file it into ``$GS_CONFIG/mappings/`` and edit it. This overrides the default internal mappings file.
The file consists of a series of ``<ACTION>=<KEYS>`` entries. For example:

.. code::

    # Help
    action.help                                 = F1
    action.help                                 = H

    # Exit
    action.exit                                 = ESC

    # Home
    action.home                                 = HOME

    # Preferences
    action.preferences                          = P

    #action.playcamera                          = C

The available actions are defined in this `I18n file here <https://gitlab.com/langurmonkey/gaiasky/blob/master/assets/i18n/gsbundle.properties#L1130>`__.

.. _keyboard-focus-free-mode:

Focus and free camera modes
---------------------------

These keyboard controls apply to the focus mode and also to the free mode.

+--------------------------------------------------------------+---------------------------------------------------+
| Key(s)                                                       | Action                                            |
+==============================================================+===================================================+
| :guilabel:`↑`                                                | Camera forward                                    |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`↓`                                                | Camera backward                                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`→`                                                | Rotate/yaw right                                  |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`←`                                                | Rotate/yaw left                                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Home`                                             | Back to Earth (or any other home object)          |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Tab`                                              | Toggle minimap                                    |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`r`                             | Reset time to current                             |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Num 0` or :guilabel:`0`                           | Free camera                                       |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Num 1` or :guilabel:`1`                           | Focus camera                                      |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Num 2` or :guilabel:`2`                           | Game mode                                         |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Num 3` or :guilabel:`3`                           | Gaia scene mode                                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Num 4` or :guilabel:`4`                           | Spacecraft mode                                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Num 5` or :guilabel:`5`                           | Gaia FOV1 camera                                  |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Num 6` or :guilabel:`6`                           | Gaia FOV2 camera                                  |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Num 7` or :guilabel:`7`                           | Gaia FOV1 and FOV2 camera                         |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`w`                             | New keyframe                                      |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`k`                             | Panorama mode                                     |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Space`                                            | Pause/resume time                                 |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`F1`                                               | Help dialog                                       |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`F5`                                               | Take screenshot                                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`F6`                                               | Start/stop frame output mode                      |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`F11`                                              | Toggle fullscreen/windowed mode                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`f` or :guilabel:`f`            | Search dialog                                     |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Esc` or :guilabel:`q`                             | Quit application                                  |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`p`                                                | Open preferences dialog                           |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`h`                                                | Open help dialog                                  |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`r`                                                | Run script dialog                                 |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`c`                                                | Run camera path file dialog                       |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`-`                                                | Decrease limiting magnitude                       |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`+`                                                | Increase limiting magnitude                       |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`,`                                                | Divide time warp by two                           |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`.`                                                | Double time warp                                  |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`*`                                                | Reset limiting magnitude                          |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`b`                            | Toggle constellation boundaries                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`c`                            | Toggle constellation lines                        |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`e`                            | Toggle ecliptic grid                              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`g`                            | Toggle galactic grid                              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`l`                            | Toggle labels                                     |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`m`                            | Toggle moons                                      |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`o`                            | Toggle orbits                                     |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`p`                            | Toggle planets                                    |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`q`                            | Toggle equatorial grid                            |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`s`                            | Toggle stars                                      |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`t`                            | Toggle satellites                                 |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`v`                            | Toggle star clusters                              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`h`                            | Toggle meshes                                     |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`u`                            | Expand/collapse controls window                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`u`                             | Toggle UI completely (hide/show user interface)   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`d`                             | Toggle debug info                                 |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`s`                             | Toggle stereoscopic mode                          |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`Shift` + :guilabel:`s`         | Switch between stereoscopic profiles              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`k`                             | Toggle 360 panorama mode                          |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`Shift` + :guilabel:`k`         | Switch between 360 projections                    |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`Shift` + :guilabel:`g`         | Toggle galaxy renderer                            |
+--------------------------------------------------------------+---------------------------------------------------+

.. _keyboard-spacecraft-mode:

Spacecraft mode
---------------

These controls apply only to the spacecraft mode.

+--------------------------------------------------------------+---------------------------------------------------+
| Key(s)                                                       | Action                                            |
+==============================================================+===================================================+
| :guilabel:`w`                                                | Apply forward thrust                              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`s`                                                | Apply backward thrust                             |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`a`                                                | Roll to the left                                  |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`d`                                                | Roll to the right                                 |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`k`                                                | Stop spaceship automatically                      |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`l`                                                | Stabilize spaceship automatically                 |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`↑`                                                | Decrease pitch angle                              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`↓`                                                | Increase pitch angle                              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`←`                                                | Increase yaw angle                                |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`→`                                                | Decrease yaw angle                                |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`PgUp`                                             | Increase engine power by a factor of 10           |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`PgDown`                                           | Decrease engine power by a factor of 10           |
+--------------------------------------------------------------+---------------------------------------------------+

.. _mouse-controls:

Mouse controls
==============

Here are the default mouse controls for the focus and free :ref:`camera-modes`. The other modes do not have mouse controls.

.. _mouse-focus-mode:

Focus mode
----------

+----------------------------------------+-----------------------------------------------------------------+
| Mouse + keys                           | Action                                                          |
+========================================+=================================================================+
| `L-MOUSE DOUBLE CLICK`                 | Select focus object                                             |
+----------------------------------------+-----------------------------------------------------------------+
| `L-MOUSE SINGLE CLICK`                 | Stop all rotation and translation movement                      |
+----------------------------------------+-----------------------------------------------------------------+
| `L-MOUSE` + `DRAG`                     | Apply rotation around focus                                     |
+----------------------------------------+-----------------------------------------------------------------+
| `L-MOUSE` + :guilabel:`Shift` + `DRAG` | Camera roll                                                     |
+----------------------------------------+-----------------------------------------------------------------+
| `R-MOUSE` + `DRAG`                     | Pan view freely from focus                                      |
+----------------------------------------+-----------------------------------------------------------------+
| `M-MOUSE` + `DRAG` or `WHEEL`          | Move towards/away from focus                                    |
+----------------------------------------+-----------------------------------------------------------------+

.. _mouse-free-mode:

Free mode
---------

+----------------------------------------+-----------------------------------------------------------------+
| Mouse + keys                           | Action                                                          |
+========================================+=================================================================+
| `L-MOUSE DOUBLE CLICK`                 | Select object as focus (changes to focus mode)                  |
+----------------------------------------+-----------------------------------------------------------------+
| `L-MOUSE SINGLE CLICK`                 | Stop all rotation and translation movement                      |
+----------------------------------------+-----------------------------------------------------------------+
| `L-MOUSE` + `DRAG`                     | Pan view                                                        |
+----------------------------------------+-----------------------------------------------------------------+
| `L-MOUSE` + :guilabel:`Shift` + `DRAG` | Camera roll                                                     |
+----------------------------------------+-----------------------------------------------------------------+
| `M-MOUSE` + `DRAG` or `WHEEL`          | Forward/backward movement                                       |
+----------------------------------------+-----------------------------------------------------------------+

.. _mouse-game-mode:

Game mode
---------

Use the mouse to look around and :guilabel:`wasd` to move.

Gamepad controls
================

Gaia Sky supports (as of version ``1.5.0``) mappings for different controller types.
However, so far only the mappings files for the **Xbox 360 controller** and the **PS3 controller** are provided. 

Sometimes there are differences between the axes and buttons codes for the same controller device between operating systems. To solve
this issue, we offer a way to describe operating system specific mappings. To do so, create a new mappings file with the format
``[controller_name].[os_family].controller``, where ``os_family`` is ``linux``, ``win``, ``macos``, ``unix`` or ``solaris``. If the
mappings for the given file name and OS family are found, they will be used. Otherwise, the file defined in the configuration file is used.
For example, if we have the file ``xbox360.controller`` is defined in the configuration file, the system will look up ``xbox360.win.controller`` if on Windows, 
``xbox360.linux.controller`` if on Linux, and so on. If found, the file is used. Otherwise, the default ``xbox360.controller`` file is used. Gaia Sky
provides the default ``xbox360.controller`` file, which defines the Linux mappings, and also the Windows mappings ``xbox360.win.controller``.

The mappings files (see `here <https://gitlab.com/langurmonkey/gaiasky/blob/master/assets/mappings/xbox360.controller>`__)
must be in the ``$GS_CONFIG/mappings`` (see :ref:`folders <folders>`) folder, and basically assign the button and axis codes for the particular
controller to the actions.

.. code:: 

	# AXES
	
	axis.roll=3
	axis.pitch=1
	axis.yaw=0
	axis.move=4
	axis.velocityup=5
	axis.velocitydown=2
	
	# BUTTONS
	
	button.velocityup=2
	button.velocitydown=0
	button.velocitytenth=5
	button.velocityhalf=4



The actions depend on the current camera
mode (focus, free, spacecraft), and are described below.

Creating mappings files for new controllers
-------------------------------------------

As of version ``1.5.1`` a new controller debug mode has been added to help **create new mappings files**. This mode prints to the log all key press and release events with their respective key codes, as well as trigger events, values and codes. It also prints controller connection and disconnection events.

In order to enable the controller debug mode, set the property ``controls.debugmode=true`` in the ``$GS_CONFIG/global.properties`` file.

Put your new files in ``$GS_CONFIG/mappings/``. The name of the file should be ``[controller brand and model].mappings``. For example, ``xboxone.mappings`` or ``logitech_f310.mappings``.

Please, if you create mappings files for new game controllers, create a pull request in the `gaiasky gitlab <https://gitlab.com/langurmonkey/gaiasky/pulls>`__ so that the community can benefit.


.. _gamepad-focus-mode:

Focus mode
----------

.. figure:: img/controller/xbox-focus.png
   :alt: Xbox 360 controller focus mode

   Xbox 360 controller in focus mode


+------------------------------+-----------------------------------------+
| Property                     | Action                                  |
+==============================+=========================================+
| ``button.velocityhalf``      | Hold to apply ``0.5`` factor to speed   |
+------------------------------+-----------------------------------------+
| ``button.velocitytenth``     | Hold to apply ``0.1`` factor to speed   |
+------------------------------+-----------------------------------------+
| ``axis.velocitydown``        | Move away from focus                    |
+------------------------------+-----------------------------------------+
| ``axis.velocityup``          | Move towards focus                      |
+------------------------------+-----------------------------------------+
| ``axis.yaw``                 | Horizontal rotation around focus        |
+------------------------------+-----------------------------------------+
| ``axis.pitch``               | Vertical rotation around focus          |
+------------------------------+-----------------------------------------+
| ``axis.roll``                | Roll right and left                     |
+------------------------------+-----------------------------------------+
| ``axis.move``                | Move towards or away from focus         |
+------------------------------+-----------------------------------------+
| ``button.velocityup``        | Move towards focus                      |
+------------------------------+-----------------------------------------+
| ``button.velocitydown``      | Move away from focus                    |
+------------------------------+-----------------------------------------+


.. _gamepad-free-mode:

Free camera mode
----------------

+------------------------------+-----------------------------------------+
| Axis/button                  | Action                                  |
+==============================+=========================================+
| ``button.velocityhalf``      | Hold to apply ``0.5`` factor to speed   |
+------------------------------+-----------------------------------------+
| ``button.velocitytenth``     | Hold to apply ``0.1`` factor to speed   |
+------------------------------+-----------------------------------------+
| ``axis.velocitydown``        | Move away from focus                    |
+------------------------------+-----------------------------------------+
| ``axis.velocityup``          | Move towards focus                      |
+------------------------------+-----------------------------------------+
| ``axis.yaw``                 | Yaw right and left                      |
+------------------------------+-----------------------------------------+
| ``axis.pitch``               | Pitch up and down                       |
+------------------------------+-----------------------------------------+
| ``axis.roll``                | Move sideways                           |
+------------------------------+-----------------------------------------+
| ``axis.move``                | Move forward and backward               |
+------------------------------+-----------------------------------------+
| ``button.velocityup``        | Move towards focus                      |
+------------------------------+-----------------------------------------+
| ``button.velocitydown``      | Move away from focus                    |
+------------------------------+-----------------------------------------+

.. _gamepad-spacecraft-mode:

Spacecraft mode
---------------

+------------------------------+----------------------------------+
| Axis/button                  | Action                           |
+==============================+==================================+
| ``button.velocityhalf``      | Stabilise spacecraft rotations   |
+------------------------------+----------------------------------+
| ``button.velocitytenth``     | Stop spacecraft                  |
+------------------------------+----------------------------------+
| ``axis.velocitydown``        | Apply backward thrust            |
+------------------------------+----------------------------------+
| ``axis.velocityup``          | Apply forward thrust             |
+------------------------------+----------------------------------+
| ``axis.yaw``                 | Yaw right and left               |
+------------------------------+----------------------------------+
| ``axis.pitch``               | Pitch up and down                |
+------------------------------+----------------------------------+
| ``axis.roll``                | Roll right and left              |
+------------------------------+----------------------------------+
| ``axis.move``                | None                             |               
+------------------------------+----------------------------------+
| ``button.velocityup``        | Increase engine power            |
+------------------------------+----------------------------------+
| ``button.velocitydown``      | Decrease engine power            |
+------------------------------+----------------------------------+

Touch controls
==============

No mobile version yet.
