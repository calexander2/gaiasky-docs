.. _graphics-conf:

Graphics configuration
**********************

Most of the graphics settings can be adjusted using the ``Preferences dialog``.

Resolution and mode
===================

You can find the ``Resolution and mode`` configuration under the
``Graphics`` tab. 

- **Display mode** -- select between fullscreen mode and windowed mode.
  In the case of full screen, you can choose the resolution 
  from a list of supported resolutions in a drop down menu. If you choose 
  windowed mode, you can enter the resolution you want. You can also 
  choose whether the window should be resizable or not. In order to switch 
  from full screen mode to windowed mode during the execution, use the key :guilabel:`F11`.

- **V-sync** -- enable v-sync to limit the frame rate to the refresh rate of your 
  monitor. In some cases this may help reducing tearing.

- **Maximum frame rate** -- it is possible to set a maximum frame rate by ticking this checkbox
  and entering a positive integer value. The frame rate will be capped to that value.


Graphics settings
=================

.. _graphics-quality-setting:

Graphics quality
----------------

This setting governs the size of the textures, the complexity of the
models and also the quality of the graphical effects (``light glow``,
``lens flare``, etc.). Here are the differences:

*  **Low** -- very low resolution textures, mostly 1K (1024x512), and fewer sample counts for the visual effects than in higher quality settings.
*  **Normal** -- moderately low resolution textures (2K when available). The graphical effects use a reasonable amount of quality for nice visuals without compromising the performance too much.
*  **High** -- high-resolution 4K (3840x2160) textures. Graphical effects use a large number of samples.
*  **Ultra** -- very high resolution textures (8K, 16K, etc.).

.. _graphics-antialiasing:

Antialiasing
------------

In the ``Graphics`` tab you can also find the antialiasing
configuration. Applying antialiasing removes the jagged edges of the
scene and makes it look better. However, it does not come free of cost,
and usually has a penalty on the frames per second (FPS). There are four
main options, described below.

Find more information on antialiasing in the :ref:`performance-antialiasing` section.

**No Antialiasing**

If you choose this no antialiasing will be applied, and therefore you
will probably see jagged edges around models. This has no penalty on
either the CPU or the GPU. If want you enable antialiasing with
``override application settings`` in your graphics card driver
configuration program, you can leave the application antialiasing
setting to off.

**FXAA -- Fast Approximate Antialiasing**

This is a post-processing antialiasing filter which is very fast and produces
very good results. The performance hit depends on how
fast your graphics card is, but it is *generally low*. Since it is a post-processing effect, this will
work also when you take screenshots or output the frames.
As of Gaia Sky ``2.2.5``, FXAA is activated by default.
Here is more info on FXAA_.

.. _FXAA: http://en.wikipedia.org/wiki/Fast\_approximate\_anti-aliasing

**NFAA -- Normal Field Antialiasing**

This is yet another post-processing antialiasing technique. It is based
on generating a normal map to detect the edges for later smoothing. It
may look better on some devices and the penalty in FPS is small. It will
also work for the screenshots and frame outputs.

Orbit style
-----------

Style to render the orbits.

- **Use line style** -- use whatever style is defined in the ``Line style`` setting.
- **GPU VBOs** -- very fast, but slightly inaccurate and bad-looking. Enable for performance,
  as everything is done in the GPU.

Line style
----------

Select the line rendering backend.

- **GL lines** -- use the line primitives offered by the graphics driver. Quite fast.
- **Polyline quadstrips** -- use polygon lines. Better looking but slower.

Bloom effect
------------

Select the amount of bloom to apply to the scene.

Lens flare
----------

Activate the pseudo lens flare effect.

Motion blur
-----------

Activate the motion blur effect.


.. _elevation-graphics-conf:

Elevation representation
========================

Choose the way elevation (height) is represented in Gaia Sky.

- **Tessellation** -- use geometry subdivision.
- **Parallax mapping** -- use parallax mapping in the fragment shaders.
- **None** -- do not represent elevation.

Shadows
=======

Enable or disable shadows, and choose their properties.

- **Shadow map resolution** -- choose the resolution of the shadow map textures to use.
- **# shadows** -- how many shadows are active at a time in the scene.

Image levels
============

Control the image levels

- **Brightness** -- overall brightness of the image.
- **Contrast** -- overall contrast of the image.
- **Hue** -- hue value of the image.
- **Saturation** -- saturation value of the image.
- **Gamma correction** -- gamma correction value of the image. This should be calibrated with your monitor.
- **HDR tone mapping type** -- tone mapping algorithm to use. Choose ``Automatic`` to use a real-time adjusting 
  mode based on the overall lightness of the image. All the others are static algorithms.


