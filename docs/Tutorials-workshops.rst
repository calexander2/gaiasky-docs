.. _tutorials:

Tutorials and workshops
***********************

This page gathers a list of the tutorials and workshops organised around Gaia Sky.

.. toctree::
   :maxdepth: 3
    
   workshops/dpac-plenary-hdb-2020
