.. _faq:

Frequently Asked Questions
**************************

Q: What is the base-data package?
=================================

The ``base-data`` package is required for Gaia Sky to run and contains basically the Solar System data (textures, models, orbits and attitudes of planets, moons, satellites, etc.). You can't run Gaia Sky without the ``base-data`` package. 

Q: Why do you have two different download pages?
================================================

We list the most important downloads in the official webpage of Gaia Sky (`here <https://zah.uni-heidelberg.de/institutes/ari/gaia/outreach/gaiasky/downloads/>`__) for convenience. The server listing (`here <http://gaia.ari.uni-heidelberg.de/gaiasky/files/autodownload/>`__) provides access to current and old releases.

At the end of the day, if you use the download manager of Gaia Sky, you will never see any of these. If you want to download the data manually, you can do so using either page.

Q: Why so many DR2 catalogs?
============================

We offer 9 different catalogs based on DR2. **Only one** should be used at a time, as they are different subsets of the same data, meaning that smaller DR2 catalogs are contained in larger DR2 catalogs. For exmaple, the catalog ``dr2-default`` is contained in ``dr2-large``. ``dr2-verysmall`` is contained in ``dr2-default``, and so on. We offer so many to give the opportunity to explore the DR2 data to everyone. Even if you have a potato PC, you can still run Gaia Sky with the ``dr2-verysmall`` dataset, which only contains the very best stars in terms of parallax relative error. If you have a more capable machine, you can explore larger and larger slices and get more stars in.

Q: Gaia Sky crashes on start-up, what to do?
============================================

We need a log. If you are using version ``2.2.0`` or newer, or the development version on the ``master`` branch, crash reports are saved in ``~/.local/share/gaiasky/crashreports`` (Linux) or ``~/.gaiasky/crashreports`` (Windows and macOS). Files starting with ``gaiasky_crash_*`` are crash reports, while files starting with ``gaiasky_log_*`` are full logs. Send us the relevant files.

If you are using another version of Gaia Sky (``2.1.7`` or older), getting a log differs depending on your Operating System.

On **Linux**, just run Gaia Sky from the command line and copy the log.

.. code:: console

    $  gaiasky

On **Windows**, files named ``output.log`` and ``error.log`` should be created in the installation folder of Gaia Sky. Check if they exist and, if so, attach them to the bug report. Otherwise, just open Power Shell, navigate to the installation folder and run the ``gaiasky.cmd`` script. The log will be printed in the Power Shell window.

On **macOS**, open a Terminal window and write this:

.. code:: console
    
    $  cd /Applications/Gaia\ Sky.app/Contenst/java/app
    $  chmod u+x ./gaiasky
    $  ./gaiasky

This will launch Gaia Sky in the terminal. Copy the log and paste it in the bug report. `Here is a video <https://youtu.be/B1wbzN-Zk_k>`__ demonstrating how to do this on macOS.

Once you have a log, create a bug report `here <https://gitlab.com/langurmonkey/gaiasky/issues>`__, attach the log, and we'll get to it ASAP.

Q: I can't see the elevation data on Earth or other planets
===========================================================

First, make sure you are using at least version `2.2.0`. Then, make sure that your graphics card supports tessellation (OpenGL 4.x). Then, download the High-resolution texture pack using the download manager and select ``High`` or ``Ultra`` in graphics quality. This is not strictly necessary, but it is much better to use higher resolution data if possible.

Q: Can I contribute?
====================

Yes. You can contribute translations (currently EN, DE, CA, FR, SK, ES are available) or code. Please, have a look at the `contributing guidelines <https://gitlab.com/langurmonkey/gaiasky/blob/master/CONTRIBUTING.md>`__.

Q: I like Gaia Sky so much, can I donate to contribute to the project?
======================================================================

Thanks, but no.
