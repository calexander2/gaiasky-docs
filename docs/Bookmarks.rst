.. _bookmarks:

Bookmarks
*********

Gaia Sky offers a way to keep your favorite objects organized with bookmarks. Bookmarks are
laid out in a folder tree. Bookmarks can either be in the root level or in any of the
folders, which can also be nested.
Any focusable object can be added to the bookmarks tree by simply clicking on
the star |star| next to the object's name when in focus. Once the object is in the bookmarks,
the star will brighten up with a clear white color (depending on the UI theme).

Clicking on a bookmark selects the object and makes it the current focus. It also changes the
camera to focus mode if needed. If the object does not exist, then nothing will happen. If the object
exists but is not visible, a small text appears below the bookmarks tree notifying the user.

.. |star| image:: img/ui/iconic-star.png


.. figure:: img/screenshots/bookmarks.jpg
  :alt: Bookmarks pane

  The bookmarks pane

New bookmarks are added at the end of the root level (top of the folder structure). Move bookmarks
around with the context menu that pops up when right clicking on them. This context menu also
provides controls to create new folders and to delete bookmarks. Bookmarks can also be deleted by
clicking on the star next to the name. Once the bookmark is removed, the star's color changes to gray.

Bookmarks are saved to the file ``$GS_CONFIG/bookmarks/bookmarks.txt`` (see :ref:`the folders section <folders>`). The format of the file is very straightforward: each non-blank and non-commented (preceded by ``#``) line contains a bookmark in the form ``folder1/folder2/[...]/object-name``. You can edit this file directly or share it with others.

This is a valid bookmarks file:


.. code:: text

    # Bookmarks file for Gaia Sky, one bookmark per line, folder separator: '/', comments: '#'
    Stars/Sirius
    Stars/Betelgeuse
    Star Clusters/Pleiades
    Star Clusters/Hyades
    Satellites/Gaia
    Solar System/Sun
    Solar System/Earth
    Solar System/Mercury
    Solar System/Venus
    Solar System/Mars
    Solar System/Phobos
    Solar System/Deimos
    Solar System/Jupiter
    Solar System/Saturn
    Solar System/Uranus
    Solar System/Neptune
    Solar System/Moons/Moon
    Solar System/Moons/Phobos
    Solar System/Moons/Deimos
    Solar System/Moons/Amalthea
    Solar System/Moons/Io
    Solar System/Moons/Europa
    Solar System/Moons/Ganymede
    Solar System/Moons/Callisto
    Solar System/Moons/Prometheus
    Solar System/Moons/Titan
    Solar System/Moons/Rhea
    Solar System/Moons/Dione
    Solar System/Moons/Tethys
    Solar System/Moons/Enceladus
    Solar System/Moons/Mimas
    Solar System/Moons/Janus

