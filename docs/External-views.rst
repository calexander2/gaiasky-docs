.. _external-views:

External views
**************

Gaia Sky offers a mode to create an additional window with an external view of 
the current scene and no user interface controls. This may be useful when presenting
or in order to project the view to an external screen or dome.

In order to create the view, just use the ``-e`` or ``--externalview`` flags when launching
Gaia Sky.

.. code:: console

    $  gaiasky -e


The external view contains a copy of the same frame buffer rendered in the main view. The
scene is not re-rendered (for now), so increasing the size of the external window
won't increase its base resolution. The original aspect ratio is maintained in the
external view to avoid stretching the image.


.. hint:: Enable the external view at launch with the flag ``-e`` or ``--externalview``.

.. figure:: img/external-view.jpg
  :width: 100%

  External view using planetarium mode
