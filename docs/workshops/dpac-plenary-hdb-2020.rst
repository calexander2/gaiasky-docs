.. _dpac-plenary-hdb-2020:

Video production workshop
=========================

**The DPAC meeting has been cancelled due to the Coronavirus.**

This page contains the workshop on video production with Gaia Sky given at the DPAC plenary meeting in Heidelberg on March 4, 2020.

The main aim of this workshop is to provide a birds eye view of the capabilities of Gaia Sky when it comes to producing high-quality videos. 


*Estimated duration:* 1.5 hours

Before starting...
------------------

In order to follow the workshop it is recommended, albeit not required, to have a local installation of Gaia Sky so that you can explore and try out the teachings for yourself. In order to install Gaia Sky, follow the instructions in :ref:`installation`.

When you start Gaia Sky for the first time you will be greeted with a data downloader window. Please download the following data packs:

- ``default-data`` -- Contains the Solar System and some other basic data
- ``catalog-dr2-verysmall`` or ``catalog-hipparcos-newred`` -- The star catalog
- ``catalog-nbg`` -- Neraby Galaxies Catalog
- ``catalog-sdss-12`` (small) or ``catalog-sdss-14`` (large) -- Sloan Digital Sky Survey catalog
- ``catalog-nebulae`` -- Some nebulae images

Then click on 'Start'.


The User Interface of Gaia Sky
------------------------------

The User Interface of Gaia Sky consists of basically two components: Keyboard shortcuts and a Graphical User Interface in the form of the controls window. 

.. figure:: img/ui.jpg
   :alt: Gaia Sky User Unterface

   Gaia Sky User Interface with the most useful functions

* The UI is descibed in detail in :ref:`user-interface`
* The most common key bindings are listed in :ref:`controls`

Loading datasets
----------------

Gaia Sky supports the loading of additional datasets at runtime. Right now, only the VOTable and CSV formats are tested, even though others may also be found to work in some cases. Gaia Sky needs some metadata, in the form of UCDs or column names, in order to parse the data correctly. Refere to the :ref:`stil-data-provider` section for more information on how to prepare your dataset for Gaia Sky.

The datasets loaded in Gaia Sky at a certain moment can be found in the :ref:`datasets` pane of the controls window.

.. figure:: img/ds.jpg
   :alt: Datasets pane

   Datasets pane of Gaia Sky


There are three main ways to load new datasets into Gaia Sky:

* The user interface |dsload| button
* Through SAMP
* Via scripting (addressed later on in the workshop)


**Loading a dataset from the UI** -- Go ahead and remove the star catalog (either DR2 dataset or hipparcos) by clicking on the |binicon| icon. Now, download a raw Hipparcos dataset from here, click on the |dsload| icon and select the file to start the loading. In a few moments the Hipparcos new reduction dataset should be loaded into Gaia Sky.

**Loading a dataset via SAMP** -- This section presupposes that Topcat is installed in the machine and that the user knows how to use it to connect to the VO to get some data. The following video demonstrates how to do this (`mirror <https://youtu.be/sc0q-VbeoPE>`__):

.. figure:: img/samp.jpg
	:width: 400px
	:align: center
	:target: ../_images/tap-load-topcat.mp4

	Loading a dataset from Topcat through SAMP (click for video)


**Loading a dataset via scripting** -- Wait for the scripting section to come.


.. |binicon| image:: ../img/ui/bin-icon.png
.. |dsload| image:: ../img/ui/open-icon.png

Camrecorder: recording and playing back camera files
----------------------------------------------------

Gaia Sky includes a camrecorder feature to make recording and playing back camera files very easy. This comes in handy if you want to showcase a certain itinerary through a dataset, for example.

**Recording a camera path** -- The camrecorder will capture the camera state at every frame and save it into a ``.gsc`` (for Gaia Sky camera) file. You can start a recording by clicking on the |rec| icon in the camera pane of the controls window. Once the recording mode is active, the icon will turn red |recred|. Click on it again in order to stop recording and save the camera file to disk with an auto-generated file name (default location is ``$GS_DATA/camera`` (see :ref:`folders <folders>`).

**Playing a camera path** -- In order to playback a previously recorded ``.gsc`` camera file, click on the |play| icon and select the desired camera path. The recording will start immediately.

.. hint:: **Mind the FPS!** The camera recording system stores the position of the camera for every frame! It is important that recording and playback are done with the same (stable) frame rate. To set the target recording frame rate, edit the "Target FPS" field in the camrecorder settings of the preferences window. That will make sure the camera path is using the right frame rate. In order to play back the camera file at the right frame rate, you can edit the "Maximum frame rate" input in the graphics settings of the preferences window.

.. figure:: img/camrec.jpg
   :alt: Camrecorder

   Location of the controls of the camrecorder in Gaia Sky

.. |rec| image:: ../img/ui/rec-icon-gray.png
.. |recred| image:: ../img/ui/rec-icon-red.png
.. |play| image:: ../img/ui/play-icon.png

More information on camera paths in Gaia Sky can be found in :ref:`the camera paths section <camera-paths>`.

Keyframe system
~~~~~~~~~~~~~~~

The camrecorder offers an additional way to define camera paths based on keyframes. Essentially, the user defines the position and orientation of the camera at certain times and the system generates the camera path from these definitions. Gaia Sky incorporates a whole keyframe definition system, which is out of the scope of this tutorial.

As a very short preview, in order to bring up the keyframes window to start defining a camera path, click on the icon |keyframes|. 

.. |keyframes| image:: ../img/ui/rec-key-icon-gray.png

More information on the keyframe system can be found in the :ref:`keyframe system subsection <keyframe-system>` of the docs.

Frame output system
-------------------

In order to create high-quality videos, Gaia Sky offers the possibility to export every single still frame to an image file. The resolution of these still frames can be set independently of the current screen resolution.

You can start the frame output system by pressing :guilabel:`F6`. Once active, the frame rate will go down (each frame is being saved to disk). The save location of the still frame images is, by default, ``$GS_DATA/frames/[prefix]_[num].jpg``, where ``[prefix]`` is an arbitrary string that can be defined in the preferences. The save location, mode (simple or advanced), and the resolution can also be defined in the preferences.

.. figure:: img/frameoutput.jpg
   :alt: Frame output

   The configuration screen for the frame output system

Once we have the still frame images, we can convert them to a video using ``ffmpeg`` or any other encoding software. Additional information on how to convert the still frames to a video can be found in the :ref:`capturing videos section <capture-videos>`.

Scripting
---------

This section includes a **hands-on session** inspecting pre-existing scripts and writing new ones to later run them on Gaia Sky.

More information on the scripting system can be found in the :ref:`scripting section <scripting>`.

- Scripting **API specification**:

   - `2.2.4-1 <https://gitlab.com/langurmonkey/gaiasky/blob/2.2.4-1/core/src/gaiasky/script/IScriptingInterface.java>`__.
   - `development branch <https://gitlab.com/langurmonkey/gaiasky/blob/2.2.4-1/core/src/gaiasky/script/IScriptingInterface.java>`__.
- Interesting **showcase scripts** can be found `here <http://gitlab.com/langurmonkey/gaiasky/tree/master/assets/scripts/showcases>`__.
- Basic **testing scripts** can be found `here <http://gitlab.com/langurmonkey/gaiasky/tree/master/assets/scripts/tests>`__.



