.. _reference-system:

Internal reference system
*************************

The internal cartesian reference system is described as follows:
:math:`XZ` is the equatorial plane. :math:`Z` points towards the 
vernal equinox point, Aries (♈) where :math:`ra=0^\circ`. :math:`Y` 
points towards the north celestial pole (:math:`dec=90^\circ`). :math:`X` 
is perpendicular to both :math:`Z` and :math:`Y` and points to :math:`ra=90^\circ`.

.. figure:: img/refsys.png
   :alt: Gaia Sky reference system

   Gaia Sky reference system

All the positions and orientations of the entities in the scene are at
some point converted to this reference system for representation. The
same happens with the orientation sensor data in mobile devices.
