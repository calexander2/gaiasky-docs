.. _configuration:

Settings and configuration
**************************

Gaia Sky can be configured using the on-screen control panel and preferences window.
Bring up the preferences window by clicking on the preferences icon |prefs-icon| in
the Controls pane.
However, very few features are not represented in the GUI, so you may need
to dive deep into the :ref:`properties file <properties-file>`.

.. figure:: img/ui/prefs/prefs-data.jpg
  :width: 100%

  The graphics settings in Gaia Sky


.. |prefs-icon| image:: img/ui/prefs-s-icon.png


.. _config-graphics:

Graphics settings
=================

Please refer to the :ref:`graphics-conf` chapter.

.. _user-interface-config:

Interface settings
==================

The ``Interface settings`` section allows the user to set the language and the
theme of the user interface.

- **Language** -- choose the language of the interface. Changes are applied immediately after clicking on ``Save preferences``.
- **Interface theme** -- select the UI skin or theme. The available themes are:

    -  ``dark-green``, black and green theme.
    -  ``dark-blue``, black and blue theme.
    -  ``dark-orange``, orange and blue theme.
    -  ``bright-green``, a bright theme with greenish tones.
    -  ``night-red``, a red theme for low-light environments.

- **Scale UI (HiDPI)** -- all themes are also available for HiDPI (high pixel density) screens. To enable this, check the `Scale UI (HiDPI)` checkbox in the Preferences dialog, Interface tab, or add a the ``-x2`` suffix to the theme name in the properties file. 
- **Display pointer coordinates** -- display the coordinates of the pointer, either sky coordinates (equatorial), or latitude and longitude on a planet. The coordinates are shown at the bottom and right edges of the screen, aligned with the pointer.
- **Minimap size** -- adjust the base size of the minimap frame buffers. You can bring up the minimap by clicking on the minimap icon |map-icon| or by pressing :guilabel:`Tab`.
- **Crosshair** -- adjust the visibility of the different crosshairs and markers.

    - **Focus marker** -- mark the location of the current focus object.
    - **Closest object marker** -- mark the location of the closest object to the camera.
    - **Home object marker** -- mark the location of the home object, defined in the configuration file.

- **Pointer guides** -- vertical and horizontal guides spanning the full window marking the position of the pointer.

    - **Display pointer guides** -- enable or disable the pointer guides.
    - **Color** -- choose the color of the pointer guides.
    - **Width** -- choose the width of the pointer guide lines.

.. |map-icon| image:: img/ui/map-icon.png

Performance
===========

- **Enable multithreading** -- enable using multiple threads.
- **Number of threads** -- adjust the maximum number of threads to use.
- **Smooth transitions between levels of detail** -- fade the contents of octree nodes as they approach the visibility threshold. Improves graphical fidelity and removes pop-ins.
- **Draw distance** -- adjust the solid angle threshold for when octree nodes become visible. See the :ref:`draw-distance` section for more information.

More detailed info on performance can be found in the :ref:`performance section <performance>`.


Controls
========

You can see the key-action bindings in the ``Controls`` tab. Controls are
only editable by modifying the ``keyboard.mappings`` file inside the ``mappings``
folder of your installation. Check out the :ref:`Controls <controls>`  documentation
to know more.

.. _screenshots-configuration:

Screenshots
===========

You can take screenshots anytime when the application is running by
pressing :guilabel:`F5`. There are two screenshot modes available: \*
``Simple``, the classic screenshot of what is currently on screen, with
the same resolution. \* ``Advanced``, where you can define the
resolution of the screenshots.
Additionally, the output format (either ``JPG`` or ``PNG``) and the quality (in
case of ``JPG`` format) can also be set in the configuration file, usually
located in ``$GS_CONFIG/global.properties`` (see :ref:`folders <folders>`). The keys to modify are:

*  ``screenshot.format``
*  ``screenshot.quality``

- **Screenshots save location** -- choose the location on disk where the screenshots are to be saved.
- **Mode** -- choose the screenshot mode, either ``Simple`` or ``Advanced`` (see above).
- **Screenshots size** -- adjust the resolution for the ``Advanced`` screenshots mode.

.. _frame-output-config:

Frame output
============

There is a feature in Gaia Sky that enables the output of every
frame as a ``JPG`` or ``PNG`` image. This is useful to produce videos. In order to
configure the frame output system, use the ``Frame output`` tab. There
you can select the output folder, the image prefix name, the output
image resolution (in case of ``Advanced`` mode) and the target frame rate.
Additionally, the output format (either ``JPG`` or ``PNG``) and the quality (in
case of ``JPG`` format) can also be set in the configuration file, usually
located in ``$GS_CONFIG/global.properties`` (see :ref:`folders <folders>`). The keys to modify are:

*  ``graphics.render.format``
*  ``graphics.render.quality``

.. note:: Use :guilabel:`F6` to activate the frame output mode and start saving each frame as an image. Use :guilabel:`F6` again to deactivate it.

When the program is in frame output mode, it does not run in
real time but it adjusts the internal clock to produce as many frames
per second as specified here. You have to take it into account when you
later use your favourite video encoder
(`ffmpeg <https://www.ffmpeg.org/>`__) to convert the frame images into
a video.

- **Frame save location** -- choose the location on disk where the still frames are to be saved.
- **Frame name prefix** -- choose the prefix to prepend to the still frame files.
- **Target FPS** -- target framerate of the frame output system.
- **Mode** -- choose the frame mode, either ``Simple`` or ``Advanced`` (see above).
- **Size of frames** -- adjust the resolution for the ``Advanced`` mode.
- **Reset sequence number** -- resets the frame sequence number of the current session to 0. Useful if you need to re-capture frames.

.. _camera-recording-config:

Camrecorder
===========

- **Target FPS** -- set the desired **frames per second** to capture the camera paths. If your device is not fast enough in producing the specified frame rate, the application will slow down while recording so that enough frames are captured. Same behaviour will be uploading during camera playback.

- **Activate frame output automatically** -- enable **automatic frame recording** during playback. This will automatically activate the frame output system (see :ref:`frame-output-config`) during a camera file playback.

- **Keyframe preferences** -- bring up a new dialog to adjust some preferences of the camera keyframe system. See :ref:`this section <keyframe-preferences>` for more information.

.. _360-mode-config:

Panorama mode
=============

- **Cubemap side resolution** -- define the **cube map side resolution** for the 360 mode.  With this mode a cube map will be rendered (the whole scene in all direction ``+X``, ``-X``, ``+Y``, ``-Y``, ``+Z``, ``-Z``) and then it will be transformed into a flat image using an equirectangular projection. This allows for the creation of 360 (VR) videos.


.. _planetarium-mode-config:

Planetarium mode
================

- **Aperture angle** -- adjust the aperture angle to suit your dome setup.
- **Cubemap side resolution** -- the planetarium mode also works with the cube map system used in Panorama mode, so here you can also adjust the cubemap side resolution.


.. _data-config:

Data
====

As of version ``1.0.0`` you can use the **Data** tab to select the
catalogues to load. You can select as many catalogs as you want.

.. figure:: img/ui/prefs/prefs-data.jpg
  :width: 100%

  The data settings in Gaia Sky

- **Use high accuracy positions** -- enable high accuracy positions, which uses all terms in the VSOPxx and other ephemerides algorithms.
- **Data source** -- choose the catalog to load at startup. The changes will be effective on restart.
- **Catalog selection dialog** -- choose whether the catalog selection should be displayed at startup when 1) no catalogs are currently selected, 2) always, 3) never.
- **Data download** -- bring up the data download dialog to get more datasets.

Gaia
====

- **Gaia attitude** -- you can either use the ``real satellite attitude`` (takes a while to load but will ensure that Gaia points to where it should) and the ``NSL``, which is an analytical implementation of the nominal attitude of the satellite. It behaves the same as the real thing, but the observation direction is not ensured.


System
======

- **Show debug info** -- enable and disable the debug info using the ``Show debug info`` checkbox. When the debug info is enabled, the program prints the frames per second and other useful information at the top-right of the screen.
- **Reset default settings** -- revert to the default settings. You will lose your current settings file and Gaia Sky will need to be relaunched for the changes to take effect.

